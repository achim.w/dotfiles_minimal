# About

This is a set of some minimal settings for often used programs on the commandline for easy cloning to new machines.

# HowTo

1. Clone this repository
2. Run `./setup.sh` for linking of the provided dotfiles
