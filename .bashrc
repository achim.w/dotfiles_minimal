# If not running interactively, exit this script
case $- in
	*i*) ;;
	*) return;;
esac

# append (don't overwrite) history file
shopt -s histappend

HISTSIZE=10000
HISTFILESIZE=10000

# Case-insensitive matching during filename expansion
shopt -s nocaseglob

alias ll='ls -alFh'

# Ask before overwriting files
alias cp='cp -iv'
alias mv='mv -iv'
alias rm='rm -iv'

export EDITOR="vim"
export PAGER="less"
