if has('mouse')
	set mouse=a
endif

set number
set hlsearch
set incsearch

set backup
set undofile
let &undodir = glob('~/.vim/_undo/')

set history=1000
set undolevels=1000

nnoremap Y y$

nnoremap <CR> :
vnoremap <CR> :
nnoremap <Space><Space> /\c
nnoremap <Space>q :qall
nnoremap <Space>v :vsplit
nnoremap <Space>s :split
