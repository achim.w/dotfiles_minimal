#!/bin/bash

echo ""
dotfiles_dir="$( cd "$(dirname "$0")" ; pwd -P )"

# Create undodir for permanent vim undo
vim_undo_dir=~/.vim/_undo
if [[ ! -d $vim_undo_dir ]]; then
	echo "Create vim undo dir $vim_undo_dir."
	mkdir -p $vim_undo_dir
else
	echo "Vim undo dir $vim_undo_dir already exists."
fi

# Link bashrc
newfile=$dotfiles_dir/.bashrc
oldfile=~/.bashrc
if [[ -f $oldfile ]]; then
	source_bashrc="source $newfile"
	if ! grep -q "$source_bashrc" $oldfile; then
		echo "Adding source of $newfile to $oldfile."
		echo "" >> $oldfile
		echo $source_bashrc >> $oldfile
	else
		echo "File $newfile already gets sourced in $oldfile. Skip."
	fi
elif [[ ! -h $oldfile ]]; then
		echo "File $oldfile is already a link. Skip."
else
	echo "Create link for $newfile"
	ln -s $newfile $oldfile
fi

# Link remaining dotfiles
oldsuffix=.bac
files=".inputrc .tmux.conf .vimrc"

for file in $files; do

	newfile=$dotfiles_dir/$file
	oldfile=~/$file

	if [[ ! -f $newfile ]]; then
		echo "File $newfile is missing. Skip."
		continue
	fi

	if [[ -h $oldfile ]]; then
		echo "File $oldfile is already a link. Skip."
		continue
	fi

	if [[ -f $oldfile ]]; then
		echo "Backup old config-file to ${oldfile}${oldsuffix}."
		mv $oldfile $oldfile$oldsuffix
	fi

	echo "Create link for $newfile."
	ln -s $newfile $oldfile
done

echo
echo "Done!"

